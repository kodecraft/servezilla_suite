#!/bin/bash
# 
# ***** BEGIN LICENSE BLOCK *****
# 
# Zimbra Collaboration Suite Server
# Copyright (C) 2006 Zimbra, Inc.
# 
# The contents of this file are subject to the Yahoo! Public License
# Version 1.0 ("License"); you may not use this file except in
# compliance with the License.  You may obtain a copy of the License at
# http://www.zimbra.com/license.
# 
# Software distributed under the License is distributed on an "AS IS"
# basis, WITHOUT WARRANTY OF ANY KIND, either express or implied.
# 
# ***** END LICENSE BLOCK *****
# 

PROGDIR=`dirname $0`
cd $PROGDIR
PATHDIR=`pwd`
BETA=
CLEAN=no
SYNC=no
PUBLIC=yes
OVERRIDE=no
ZIMBRA=no
TOOLS=no
GIT=1
MIRROR="http://cpan.yahoo.com/"

usage() {
	echo ""
	echo "Usage: "`basename $0`" [-b] -c [-p] [-s]" >&2
	echo "-b: Use beta software versions"
	echo "-c: Remove contents of /opt/zimbra (clean)"
	echo "-p: Use private CPAN mirror"
	echo "-s: Re-sync source before building"
	echo "-t: Install required build tools (requires sudo access for user)"
	exit 2;
}

ask() {
  PROMPT=$1
  DEFAULT=$2
  
  echo ""
  echo -n "$PROMPT [$DEFAULT] "
  read response
  
  if [ -z $response ]; then
    response=$DEFAULT 
  fi
}   

askYN() {
  PROMPT=$1
  DEFAULT=$2

  if [ "x$DEFAULT" = "xyes" -o "x$DEFAULT" = "xYes" -o "x$DEFAULT" = "xy" -o "x$DEFAULT" = "xY" ]; then
    DEFAULT="Y"
  else
    DEFAULT="N"
  fi

  while [ 1 ]; do
    ask "$PROMPT" "$DEFAULT"
    response=`echo $response | tr "[:upper:]" "[:lower:]"`
    if [ -z $response ]; then
      :
    else
      if [ $response = "yes" -o $response = "y" ]; then
        response="yes"
        break
      else
        if [ $response = "no" -o $response = "n" ]; then
          response="no"
          break
        fi
      fi
    fi
    echo "A Yes/No answer is required"
  done
}

askURL() {
  PROMPT=$1
  DEFAULT=$2

  while [ 1 ]; do
    ask "$PROMPT" "$DEFAULT"
    response=`echo $response | tr "[:upper:]" "[:lower:]"`
    if [ -z $response ]; then
      :
    else
      if [[ $response == "http://"* ]]; then
        break
      fi
    fi
    echo "A http:// formed URL is required"
  done
}

parseVersion() {
	VER=$1
	MAJOR=`echo $VER | awk -F. '{print $1}'`
	MINOR=`echo $VER | awk -F. '{print $2}'`
	PATCH=`echo $VER | awk -F. '{print $3}'`
}

if [ $# -lt 1 ]; then
	usage
fi

while [ $# -gt 0 ]; do
	case $1 in
		-b|--beta)
			BETA=1
			shift;
			;;
		-c|--clean)
			CLEAN=yes
			shift;
			;;
		-o|--override)
			OVERRIDE=yes
			shift;
			;;
		-p|--private)
			PUBLIC=no
			shift;
			;;
		-h|--help)
			usage;
			exit 0;
			;;
		-s|--sync)
			SYNC=yes
			shift;
			;;
		-z|--zimbra)
			ZIMBRA=yes
			GIT=0
			shift;
			;;
		-t|--tools)
			TOOLS=yes
			shift;
			;;
		*)
			echo "Usage: $0 -c [-s]"
			exit 1;
			;;
	esac
done

if [ x"$ZIMBRA" = x"yes" ]; then
	RELEASE=${PATHDIR%/*}
	RELEASE=${RELEASE##*/}
else
	RELEASE=`git rev-parse --abbrev-ref HEAD`
	RELEASE=${RELEASE%-foss}
	RELEASE=$(echo $RELEASE| tr '[:upper:]' '[:lower:]')
fi

if [ x$CLEAN = x"no" ]; then
	echo "WARNING: You must supply the clean option -c"
	echo "WARNING: This will completely remove the contents of /opt/zimbra from the system"
	exit 1;
fi

if [ x"$ZIMBRA" = x"yes" ]; then
  P4=`which p4`;
fi

PLAT=`$PATHDIR/../ZimbraBuild/rpmconf/Build/get_plat_tag.sh`;

if [ x$PLAT = "x" ]; then
	echo "Unknown platform, exiting."
	exit 1;
fi

if [ x$OVERRIDE = x"no" ]; then
	askYN "Proceeding will remove /opt/zimbra.  Do you wish to continue?: " "N"
	if [ $response = "no" ]; then
		echo "Exiting"
		exit 1;
	fi
fi

eval `/usr/bin/perl -V:archname`
export PERLLIB="${PATHDIR}/Perl/zimbramon/lib:${PATHDIR}/Perl/zimbramon/lib/$archname"
export PERL5LIB=${PERLLIB}

if [ x$SYNC = "xyes" ]; then
  echo "Resyncing thirdparty source for $RELEASE"
  if [ x"$ZIMBRA" = x"yes" ]; then
	cd ${PATHDIR}
	$P4 sync ... > /dev/null 
  else
	cd ${PATHDIR}/..
	git pull
  fi
fi

if [ x"$ZIMBRA" = x"yes" ]; then
  if [ x$SYNC = "xyes" ]; then
	cd ${PATHDIR}/../ZimbraBuild
	$P4 sync ... > /dev/null 
  fi
fi

mkdir -p ${PATHDIR}/../ThirdPartyBuilds/$PLAT

if [ x"$ZIMBRA" = x"yes" ]; then
  if [ x$SYNC = "xyes" ]; then
	if [ x$RELEASE != "xfrank" ]; then
		cd ${PATHDIR}/../ThirdPartyBuilds/$PLAT
		$P4 sync ... > /dev/null 
	fi
  fi
fi

if [ x"$TOOLS" = "xyes" ]; then
  BINARIES="make cmake gcc patch automake autoconf bison flex bzip2 libtool unzip perl wget"
  if [[ $PLAT = "RHEL"*"64" ]]; then
    BINARIES="$BINARIES gcc-c++ rpm-build perl-libwww-perl"
    LIBRARIES="zlib-devel ncurses-devel expat-devel popt-devel pcre-devel readline-devel bzip2-devel libaio-devel"
    sudo yum install $BINARIES $LIBRARIES
  fi
  
  if [[ $PLAT == "UBUNTU"*"64" || $PLAT == "DEBIAN"*"64" ]]; then
    BINARIES="$BINARIES g++ dpkg libwww-perl"
    LIBRARIES="libz-dev libncurses-dev libexpat-dev libpopt-dev libpcre3-dev libreadline-dev libbz2-dev libaio-dev cloog-ppl libperl-dev"
    sudo apt-get install $BINARIES $LIBRARIES
    sudo apt-get purge libltdl-devel
  fi
  sudo groupadd zimbra
  sudo useradd -d /opt/zimbra -m -g zimbra zimbra
fi

if [ x"$ZIMBRA" = x"no" ]; then
	echo "Cleaning contents of /opt/zimbra"
	if [ -d "/opt/zimbra" ]; then
		rm -rf /opt/zimbra/* 2>/dev/null
		rm -rf /opt/zimbra/.* 2>/dev/null
		mkdir -p /opt/zimbra
	fi
else
	if [ -x "/home/build/scripts/setup-build.sh" ]; then
		sudo /home/build/scripts/setup-build.sh 2>/dev/null
	else
		echo "Error: setup-build.sh missing"
		exit 1;
	fi
fi

touch /opt/zimbra/blah 2>/dev/null
RC=$?

if [ $RC -eq 1 ]; then
	echo "Error: Unable to write to /opt/zimbra"
	exit 1;
else
	rm -f /opt/zimbra/blah
fi

if [ x$PUBLIC = x"yes" ]; then
	askURL "CPAN URL?" "$MIRROR"
	MIRROR=$response
fi

cd ${PATHDIR}
rm -f make.out 2> /dev/null
make allclean > /dev/null 2>&1

if [ x$PUBLIC = x"yes" ]; then
	make all CMIRROR=$MIRROR BETA=$BETA GIT=$GIT 2>&1 | tee -a make.out
else
	make all GIT=$GIT 2>&1 | tee -a make.out
fi

mkdir -p $PATHDIR/../logs
cp -f ThirdParty.make.log $PATHDIR/../logs
cp -f Perl/ThirdParty-Perllibs.log $PATHDIR/../logs
exit 0;
