#!/usr/bin/perl
my $so=shift;
my $plevel=shift;
my $dir = "/opt/zimbra/cyrus-sasl-2.1.26.$plevel/lib/sasl2";
my $platform=`../../ZimbraBuild/rpmconf/Build/get_plat_tag.sh`;
chomp $platform;
my @files=<$dir/*.la>;
foreach my $file (@files) {
	unlink $file
}

@preserve=("libanonymous.3.so", "libcrammd5.3.so", "libdigestmd5.3.so", "libgssapiv2.3.so", "liblogin.3.so", "libotp.3.so", "libplain.3.so", "libgs2.3.so", "libscram.3.so");
foreach $prev (@preserve) {
	($name, $junk) = split(/.3.so/,$prev,2);
	$fullname = $name.".so.3.0.$so";
	rename("$dir/$prev", "$dir/$fullname");
	symlink("$fullname", "$dir/$name.so.3");
}
@preserve=("libanonymous.3.0.$so.so", "libcrammd5.3.0.$so.so", "libdigestmd5.3.0.$so.so", "libgssapiv2.3.0.$so.so", "liblogin.3.0.$so.so", "libotp.3.0.$so.so", "libplain.3.0.$so.so", "libgs2.3.0.$so.so", "libscram.3.so");
@files=<$dir/*.so>;
foreach $file (@files) {
	unlink $file
}

foreach $prev (@preserve) {
	($name, $junk) = split(/.3.0.$so.so/,$prev,2);
	$fullname = $name.".so.3.0.$so";
	symlink("$fullname", "$dir/$name.so");
}
