/*
 * ***** BEGIN LICENSE BLOCK *****
 * Zimbra Collaboration Suite Server
 * Copyright (C) 2008, 2009, 2010, 2013, 2014 Zimbra, Inc.
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software Foundation,
 * version 2 of the License.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/>.
 * ***** END LICENSE BLOCK *****
 */
package com.zimbra.cs.offline.util.yab;

/**
 * YAB field flag constants.
 */
public final class Flag {
    public static final String HOME = "home";
    public static final String WORK = "work";
    public static final String PERSONAL = "personal";
    public static final String MOBILE = "mobile";
    public static final String FAX = "fax";
    public static final String PAGER = "pager";
    public static final String YAHOOPHONE = "yahoophone";
    public static final String AOL = "aol";
    public static final String MSN = "msn";
    public static final String JABBER = "jabber";
    public static final String DOTMAC = "dotmac";
    public static final String ICQ = "icq";
    public static final String GOOGLE = "google";
    public static final String SKYPE = "skype";
    public static final String IRC = "irc";
    public static final String QQ = "qq";
    public static final String LCS = "lcs";
    public static final String EXTERNAL = "external";
    public static final String Y360 = "y360";
    public static final String PHOTO = "photo";
    public static final String BLOG = "blog";
    public static final String IBM = "ibm";
}
