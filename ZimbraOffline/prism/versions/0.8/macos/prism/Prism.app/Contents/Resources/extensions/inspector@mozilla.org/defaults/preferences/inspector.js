/* ***** BEGIN LICENSE BLOCK *****
/* Zimbra Collaboration Suite Server
/* Copyright (C) 2008, 2009, 2010, 2013, 2014 Zimbra, Inc.
/* 
/* The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "License");
/* you may not use this file except in compliance with the License. 
/* You may obtain a copy of the License at: http://www.zimbra.com/license
/* The License is based on the Mozilla Public License Version 1.1 but Sections 14 and 15 
/* have been added to cover use of software over a computer network and provide for limited attribution 
/* for the Original Developer. In addition, Exhibit A has been modified to be consistent with Exhibit B. 
/* 
/* Software distributed under the License is distributed on an "AS IS" basis, 
/* WITHOUT WARRANTY OF ANY KIND, either express or implied. 
/* See the License for the specific language governing rights and limitations under the License. 
/* The Original Code is Zimbra Open Source Web Client. 
/* The Initial Developer of the Original Code is Zimbra, Inc. 
/* All portions of the code are Copyright (C) 2008, 2009, 2010, 2013, 2014 Zimbra, Inc. All Rights Reserved. 
 * ***** END LICENSE BLOCK ***** */

pref("inspector.blink.border-color", "#CC0000");
pref("inspector.blink.border-width", 2);
pref("inspector.blink.duration", 1200);
pref("inspector.blink.on", true);
pref("inspector.blink.speed", 100);
pref("inspector.blink.invert", false);
pref("inspector.dom.showAnon", true);
pref("inspector.dom.showWhitespaceNodes", true);
pref("inspector.dom.showAccessibleNodes", false);
pref("inspector.dom.showProcessingInstructions", true);
