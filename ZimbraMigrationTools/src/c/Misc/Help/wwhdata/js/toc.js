/*
 * ***** BEGIN LICENSE BLOCK *****
 * Zimbra Collaboration Suite CSharp Client
 * Copyright (C) 2012, 2014 Zimbra, Inc.
 * 
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "License");
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at: http://www.zimbra.com/license
 * The License is based on the Mozilla Public License Version 1.1 but Sections 14 and 15 
 * have been added to cover use of software over a computer network and provide for limited attribution 
 * for the Original Developer. In addition, Exhibit A has been modified to be consistent with Exhibit B. 
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. 
 * See the License for the specific language governing rights and limitations under the License. 
 * The Original Code is Zimbra Open Source Web Client. 
 * The Initial Developer of the Original Code is Zimbra, Inc. 
 * All portions of the code are Copyright (C) 2012, 2014 Zimbra, Inc. All Rights Reserved. 
 * ***** END LICENSE BLOCK *****
 */
function  WWHBookData_AddTOCEntries(P)
{
var A=P.fN("Starting the Migration","0");
A=P.fN("Server Migration Source Information","1");
A=P.fN("User Migration Source Information","2");
A=P.fN("Destination Server Configuration","3");
A=P.fN("Destination Server Configuration","4");
A=P.fN("Selecting Options to Migrate","5");
A=P.fN("Selecting Options to Migrate","6");
A=P.fN("Selecting Users to Migrate","7");
var B=A.fN("Use Load CSV","7#1006045");
B=A.fN("Use Object Picker","7#1006048");
B=A.fN("Use LDAP Browser","7#1006072");
B=A.fN("Add Users Manually","7#1006174");
A=P.fN("Scheduling the Migration","8");
A=P.fN("Viewing Migration Results","9");
}
