/*
 * ***** BEGIN LICENSE BLOCK *****
 * Zimbra Collaboration Suite CSharp Client
 * Copyright (C) 2012, 2014 Zimbra, Inc.
 * 
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "License");
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at: http://www.zimbra.com/license
 * The License is based on the Mozilla Public License Version 1.1 but Sections 14 and 15 
 * have been added to cover use of software over a computer network and provide for limited attribution 
 * for the Original Developer. In addition, Exhibit A has been modified to be consistent with Exhibit B. 
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. 
 * See the License for the specific language governing rights and limitations under the License. 
 * The Original Code is Zimbra Open Source Web Client. 
 * The Initial Developer of the Original Code is Zimbra, Inc. 
 * All portions of the code are Copyright (C) 2012, 2014 Zimbra, Inc. All Rights Reserved. 
 * ***** END LICENSE BLOCK *****
 */
function  WWHBookData_SearchFileCount()
{
  return 1;
}

function  WWHBookData_SearchMinimumWordLength()
{
  return 3;
}

function  WWHBookData_SearchSkipWords(P)
{
P.fA("now");
P.fA("all");
P.fA("being");
P.fA("the");
P.fA("him");
P.fA("and");
P.fA("most");
P.fA("etc");
P.fA("here");
P.fA("some");
P.fA("will");
P.fA("another");
P.fA("copyright");
P.fA("that");
P.fA("which");
P.fA("would");
P.fA("corporation");
P.fA("himself");
P.fA("his");
P.fA("reserved");
P.fA("set");
P.fA("inc");
P.fA("where");
P.fA("shall");
P.fA("might");
P.fA("between");
P.fA("goes");
P.fA("got");
P.fA("too");
P.fA("more");
P.fA("never");
P.fA("had");
P.fA("said");
P.fA("while");
P.fA("you");
P.fA("them");
P.fA("then");
P.fA("did");
P.fA("came");
P.fA("into");
P.fA("other");
P.fA("who");
P.fA("also");
P.fA("should");
P.fA("those");
P.fA("was");
P.fA("any");
P.fA("are");
P.fA("let");
P.fA("like");
P.fA("must");
P.fA("since");
P.fA("because");
P.fA("get");
P.fA("what");
P.fA("both");
P.fA("these");
P.fA("way");
P.fA("yes");
P.fA("can");
P.fA("they");
P.fA("yet");
P.fA("about");
P.fA("has");
P.fA("well");
P.fA("why");
P.fA("each");
P.fA("one");
P.fA("before");
P.fA("over");
P.fA("only");
P.fA("same");
P.fA("your");
P.fA("from");
P.fA("she");
P.fA("with");
P.fA("how");
P.fA("under");
P.fA("when");
P.fA("such");
P.fA("this");
P.fA("make");
P.fA("very");
P.fA("but");
P.fA("still");
P.fA("many");
P.fA("use");
P.fA("our");
P.fA("could");
P.fA("for");
P.fA("her");
P.fA("come");
P.fA("have");
P.fA("out");
P.fA("does");
P.fA("though");
P.fA("were");
P.fA("corp");
P.fA("its");
P.fA("own");
P.fA("their");
P.fA("there");
P.fA("see");
P.fA("take");
P.fA("through");
P.fA("nor");
P.fA("off");
P.fA("much");
P.fA("not");
P.fA("rights");
P.fA("after");
P.fA("been");
P.fA("than");
}
