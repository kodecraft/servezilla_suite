/*
 * ***** BEGIN LICENSE BLOCK *****
 * Zimbra Collaboration Suite CSharp Client
 * Copyright (C) 2012, 2014 Zimbra, Inc.
 * 
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "License");
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at: http://www.zimbra.com/license
 * The License is based on the Mozilla Public License Version 1.1 but Sections 14 and 15 
 * have been added to cover use of software over a computer network and provide for limited attribution 
 * for the Original Developer. In addition, Exhibit A has been modified to be consistent with Exhibit B. 
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. 
 * See the License for the specific language governing rights and limitations under the License. 
 * The Original Code is Zimbra Open Source Web Client. 
 * The Initial Developer of the Original Code is Zimbra, Inc. 
 * All portions of the code are Copyright (C) 2012, 2014 Zimbra, Inc. All Rights Reserved. 
 * ***** END LICENSE BLOCK *****
 */
function FileData_Pairs(x)
{
x.t("email","messages");
x.t("junk","(spam)");
x.t("files","email");
x.t("files","imported");
x.t("files","meeting");
x.t("lists","imported");
x.t("lists","task");
x.t("lists","rules");
x.t("console","global");
x.t("attachments","calendar");
x.t("attachments","configure");
x.t("contact","lists");
x.t("limits","imposed");
x.t("contacts","tasks");
x.t("enabled","inspect");
x.t("dialog","box");
x.t("mta","setting");
x.t("mta","tab");
x.t("change","zcs");
x.t("inspect","log");
x.t("filters","migrating");
x.t("filters","select");
x.t("default","maximum");
x.t("comma","enter");
x.t("migrated","option");
x.t("want","skip");
x.t("want","migrate");
x.t("items","select");
x.t("items","(trash)");
x.t("size","want");
x.t("size","includes");
x.t("size","limit");
x.t("size","indicating");
x.t("size","message");
x.t("size","used");
x.t("includes","message");
x.t("lets","discard");
x.t("calendar","contacts");
x.t("calendar","data");
x.t("filter","information");
x.t("logged","message");
x.t("imported","select");
x.t("imported","options");
x.t("imported","setting");
x.t("specified","date");
x.t("account","files");
x.t("account","data");
x.t("settings","mta");
x.t("settings","imported");
x.t("settings","additional");
x.t("box","select");
x.t("box","used");
x.t("tasks","rules");
x.t("notes/journal","alerts");
x.t("skip","folders");
x.t("old","messages");
x.t("limit","note");
x.t("logging","allows");
x.t("determine","cause");
x.t("allows","extra");
x.t("personal","distribution");
x.t("extra","data");
x.t("imposed","zcs");
x.t("(spam)","folders");
x.t("migrating","specified");
x.t("migrating","migrate");
x.t("options","dialog");
x.t("options","migrate");
x.t("messages","attachments");
x.t("messages","calendar");
x.t("messages","maximum");
x.t("select","filter");
x.t("select","account");
x.t("select","migrate");
x.t("cause","failure");
x.t("maximum","size");
x.t("maximum","message");
x.t("indicating","size");
x.t("cannot","larger");
x.t("office","settings");
x.t("requests","notes/journal");
x.t("types","files");
x.t("meeting","requests");
x.t("(trash)","junk");
x.t("task","lists");
x.t("override","limits");
x.t("message","attachments");
x.t("message","default");
x.t("message","migrated");
x.t("message","size");
x.t("message","value");
x.t("option","enabled");
x.t("enter","date");
x.t("enter","names");
x.t("rules","files");
x.t("rules","office");
x.t("alerts","personal");
x.t("zcs","server");
x.t("additional","folders");
x.t("value","cannot");
x.t("value","zcs");
x.t("value","value");
x.t("value","administration");
x.t("selecting","options");
x.t("view","change");
x.t("distribution","lists");
x.t("larger","global");
x.t("separate","comma");
x.t("data","contact");
x.t("data","logged");
x.t("data","migrate");
x.t("information","migrating");
x.t("discard","old");
x.t("configure","value");
x.t("migrate","email");
x.t("migrate","items");
x.t("migrate","account");
x.t("migrate","enter");
x.t("migrate","selecting");
x.t("migrate","migrate");
x.t("migrate","verbose");
x.t("migrate","accounts");
x.t("migrate","sent");
x.t("global","mta");
x.t("global","settings");
x.t("date","lets");
x.t("date","maximum");
x.t("date","migrate");
x.t("note","override");
x.t("note","view");
x.t("administration","console");
x.t("verbose","logging");
x.t("used","rules");
x.t("used","value");
x.t("setting","filters");
x.t("setting","maximum");
x.t("deleted","items");
x.t("deleted","(trash)");
x.t("folders","filters");
x.t("folders","want");
x.t("folders","options");
x.t("folders","select");
x.t("folders","types");
x.t("folders","separate");
x.t("tab","skip");
x.t("accounts","specified");
x.t("names","folders");
x.t("sent","deleted");
x.t("log","determine");
x.t("server","maximum");
x.t("server","note");
x.t("server","setting");
}
