/*
 * ***** BEGIN LICENSE BLOCK *****
 * Zimbra Collaboration Suite CSharp Client
 * Copyright (C) 2012, 2014 Zimbra, Inc.
 * 
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "License");
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at: http://www.zimbra.com/license
 * The License is based on the Mozilla Public License Version 1.1 but Sections 14 and 15 
 * have been added to cover use of software over a computer network and provide for limited attribution 
 * for the Original Developer. In addition, Exhibit A has been modified to be consistent with Exhibit B. 
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. 
 * See the License for the specific language governing rights and limitations under the License. 
 * The Original Code is Zimbra Open Source Web Client. 
 * The Initial Developer of the Original Code is Zimbra, Inc. 
 * All portions of the code are Copyright (C) 2012, 2014 Zimbra, Inc. All Rights Reserved. 
 * ***** END LICENSE BLOCK *****
 */
function FileData_Pairs(x)
{
x.t("console","source");
x.t("saved","profile");
x.t("create","temporary");
x.t("source","dialog");
x.t("source","information");
x.t("wizard","guide");
x.t("upload","previously");
x.t("upload","profile");
x.t("dialog","box");
x.t("later","time");
x.t("administrator","account");
x.t("administrator","profile");
x.t("migration","source");
x.t("migration","wizard");
x.t("migration","details");
x.t("migration","profile");
x.t("migration","process");
x.t("populate","profile");
x.t("automatically","deleted");
x.t("details","copy");
x.t("access","profiles");
x.t("want","create");
x.t("want","migrate");
x.t("entering","allows");
x.t("entering","exchange");
x.t("found","help");
x.t("profiles","want");
x.t("complete","migration");
x.t("credentials","creating");
x.t("credentials","hostname/ip");
x.t("guide","complete");
x.t("creating","temporary");
x.t("box","enter");
x.t("account","want");
x.t("account","allows");
x.t("account","exchange");
x.t("name","administrator");
x.t("name","address");
x.t("name","exchange");
x.t("previously","saved");
x.t("profile","automatically");
x.t("profile","entering");
x.t("profile","account");
x.t("profile","fields");
x.t("profile","already");
x.t("profile","read");
x.t("profile","information");
x.t("profile","created");
x.t("allows","access");
x.t("allows","information");
x.t("address","admin");
x.t("fields","click");
x.t("hostname/ip","host");
x.t("next","load");
x.t("select","exchange");
x.t("select","outlook");
x.t("exchange","administrator");
x.t("exchange","server\u2019s");
x.t("exchange","server");
x.t("admin","name");
x.t("enter","exchange");
x.t("enter","relevant");
x.t("click","next");
x.t("click","save");
x.t("click","load");
x.t("already","exchange");
x.t("temporary","exchange");
x.t("temporary","enter");
x.t("process","click");
x.t("read","migration");
x.t("help","desk");
x.t("page","administration");
x.t("information","later");
x.t("information","populate");
x.t("information","entering");
x.t("information","select");
x.t("information","click");
x.t("information","migrate");
x.t("information","server");
x.t("data","exchange");
x.t("host","name");
x.t("save","upload");
x.t("save","profile");
x.t("save","save");
x.t("save","load");
x.t("migrate","select");
x.t("migrate","data");
x.t("administration","console");
x.t("server\u2019s","credentials");
x.t("copy","found");
x.t("created","migration");
x.t("either","exchange");
x.t("relevant","exchange");
x.t("using","either");
x.t("desk","page");
x.t("password","administrator");
x.t("password","password");
x.t("deleted","migration");
x.t("load","upload");
x.t("load","save");
x.t("outlook","profile");
x.t("server","migration");
x.t("server","credentials");
x.t("server","name");
x.t("server","exchange");
x.t("server","information");
x.t("server","using");
x.t("server","password");
}
