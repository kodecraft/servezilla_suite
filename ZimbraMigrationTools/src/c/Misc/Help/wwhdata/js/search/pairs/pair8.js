/*
 * ***** BEGIN LICENSE BLOCK *****
 * Zimbra Collaboration Suite CSharp Client
 * Copyright (C) 2012, 2014 Zimbra, Inc.
 * 
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "License");
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at: http://www.zimbra.com/license
 * The License is based on the Mozilla Public License Version 1.1 but Sections 14 and 15 
 * have been added to cover use of software over a computer network and provide for limited attribution 
 * for the Original Developer. In addition, Exhibit A has been modified to be consistent with Exhibit B. 
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. 
 * See the License for the specific language governing rights and limitations under the License. 
 * The Original Code is Zimbra Open Source Web Client. 
 * The Initial Developer of the Original Code is Zimbra, Inc. 
 * All portions of the code are Copyright (C) 2012, 2014 Zimbra, Inc. All Rights Reserved. 
 * ***** END LICENSE BLOCK *****
 */
function FileData_Pairs(x)
{
x.t("full","migration");
x.t("user","account");
x.t("user","name");
x.t("user","already");
x.t("user","accounts");
x.t("later","migrate");
x.t("later","date");
x.t("dialog","box");
x.t("field","enter");
x.t("displays","user");
x.t("displays","migrate");
x.t("icon","user");
x.t("migration","migration");
x.t("migration","start");
x.t("migration","complete");
x.t("migration","place");
x.t("migration","select");
x.t("migration","ready");
x.t("migration","process");
x.t("migration","although");
x.t("migration","scheduling");
x.t("time","select");
x.t("time","click");
x.t("time","(optional)");
x.t("time","schedule");
x.t("migrated","preview");
x.t("migrated","considered");
x.t("migrated","provisioned");
x.t("default","password");
x.t("start","selected");
x.t("preview","displays");
x.t("preview","review");
x.t("file","information");
x.t("during","migration");
x.t("greyed-out","icon");
x.t("complete","migration");
x.t("complete","provisioning");
x.t("complete","migrate");
x.t("assign","initial");
x.t("test","run");
x.t("box","user");
x.t("box","greyed-out");
x.t("box","assign");
x.t("box","red");
x.t("box","select");
x.t("box","enter");
x.t("box","click");
x.t("account","migrated");
x.t("account","zcs");
x.t("account","already");
x.t("account","created");
x.t("account","provisioned");
x.t("name","displays");
x.t("place","later");
x.t("place","zcs");
x.t("place","schedule");
x.t("completing","provisioning");
x.t("fields","dialog");
x.t("fields","migrate");
x.t("begin","migration");
x.t("selected","date");
x.t("review","accounts");
x.t("red","icon");
x.t("select","migration");
x.t("select","option");
x.t("select","schedule");
x.t("select","migrate");
x.t("changes","running");
x.t("initial","password");
x.t("task","migration");
x.t("option","migrate");
x.t("ready","click");
x.t("enter","default");
x.t("enter","date");
x.t("click","preview");
x.t("click","schedule");
x.t("click","migrate");
x.t("(optional)","migrate");
x.t("zcs","during");
x.t("zcs","schedule");
x.t("zcs","server");
x.t("username","displays");
x.t("running","complete");
x.t("already","account");
x.t("already","provisioned");
x.t("process","user");
x.t("considered","test");
x.t("although","full");
x.t("schedule","later");
x.t("schedule","dialog");
x.t("schedule","migration");
x.t("schedule","task");
x.t("schedule","schedule");
x.t("schedule","migrate");
x.t("information","migrated");
x.t("provisioning","fields");
x.t("newly","provisioned");
x.t("appropriate","changes");
x.t("migrate","dialog");
x.t("migrate","begin");
x.t("migrate","schedule");
x.t("migrate","users");
x.t("users","zcs");
x.t("date","time");
x.t("created","completing");
x.t("service","initial");
x.t("run","migration");
x.t("scheduling","migration");
x.t("class","service");
x.t("password","field");
x.t("password","newly");
x.t("password","class");
x.t("accounts","migrated");
x.t("accounts","file");
x.t("accounts","select");
x.t("provisioned","zcs");
x.t("provisioned","accounts");
x.t("server","user");
x.t("server","complete");
x.t("server","account");
x.t("server","username");
x.t("server","appropriate");
}
