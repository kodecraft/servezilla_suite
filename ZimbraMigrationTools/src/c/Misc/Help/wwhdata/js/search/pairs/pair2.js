/*
 * ***** BEGIN LICENSE BLOCK *****
 * Zimbra Collaboration Suite CSharp Client
 * Copyright (C) 2012, 2014 Zimbra, Inc.
 * 
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "License");
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at: http://www.zimbra.com/license
 * The License is based on the Mozilla Public License Version 1.1 but Sections 14 and 15 
 * have been added to cover use of software over a computer network and provide for limited attribution 
 * for the Original Developer. In addition, Exhibit A has been modified to be consistent with Exhibit B. 
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. 
 * See the License for the specific language governing rights and limitations under the License. 
 * The Original Code is Zimbra Open Source Web Client. 
 * The Initial Developer of the Original Code is Zimbra, Inc. 
 * All portions of the code are Copyright (C) 2012, 2014 Zimbra, Inc. All Rights Reserved. 
 * ***** END LICENSE BLOCK *****
 */
function FileData_Pairs(x)
{
x.t("pst","file");
x.t("console","select");
x.t("saved","profile");
x.t("source","information");
x.t("wizard","guide");
x.t("upload","previously");
x.t("upload","profile");
x.t("user","migration");
x.t("later","time");
x.t("everything","profile");
x.t("migration","source");
x.t("migration","wizard");
x.t("migration","details");
x.t("populate","profile");
x.t("migrated","pst");
x.t("migrated","click");
x.t("details","copy");
x.t("file","migrated");
x.t("file","selected");
x.t("file","read");
x.t("file","outlook");
x.t("found","help");
x.t("entering","allows");
x.t("complete","migration");
x.t("menu","everything");
x.t("guide","complete");
x.t("name","menu");
x.t("previously","saved");
x.t("profile","pst");
x.t("profile","migrated");
x.t("profile","name");
x.t("profile","fields");
x.t("profile","selected");
x.t("profile","information");
x.t("allows","information");
x.t("entire",".pst");
x.t("fields","click");
x.t("selected","entire");
x.t("selected","select");
x.t("next","load");
x.t("select","profile");
x.t("select","migrate");
x.t("click","next");
x.t("click","save");
x.t("click","load");
x.t("read","migration");
x.t("help","desk");
x.t("page","administration");
x.t("information","user");
x.t("information","later");
x.t("information","populate");
x.t("information","entering");
x.t("information","click");
x.t("information","migrate");
x.t("data","outlook");
x.t("save","upload");
x.t("save","profile");
x.t("save","save");
x.t("save","load");
x.t("migrate","data");
x.t("migrate","outlook");
x.t("administration","console");
x.t("copy","found");
x.t(".pst","file");
x.t("desk","page");
x.t("load","upload");
x.t("load","save");
x.t("outlook","profile");
}
