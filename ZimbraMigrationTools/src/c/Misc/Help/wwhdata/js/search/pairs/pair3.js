/*
 * ***** BEGIN LICENSE BLOCK *****
 * Zimbra Collaboration Suite CSharp Client
 * Copyright (C) 2012, 2014 Zimbra, Inc.
 * 
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "License");
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at: http://www.zimbra.com/license
 * The License is based on the Mozilla Public License Version 1.1 but Sections 14 and 15 
 * have been added to cover use of software over a computer network and provide for limited attribution 
 * for the Original Developer. In addition, Exhibit A has been modified to be consistent with Exhibit B. 
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. 
 * See the License for the specific language governing rights and limitations under the License. 
 * The Original Code is Zimbra Open Source Web Client. 
 * The Initial Developer of the Original Code is Zimbra, Inc. 
 * All portions of the code are Copyright (C) 2012, 2014 Zimbra, Inc. All Rights Reserved. 
 * ***** END LICENSE BLOCK *****
 */
function FileData_Pairs(x)
{
x.t("dialog","enter");
x.t("administrator","account");
x.t("migrated","enter");
x.t("7071","port");
x.t("credentials","destination");
x.t("credentials","hostname");
x.t("name","administrator");
x.t("name","destination");
x.t("account","destination");
x.t("destination","dialog");
x.t("destination","zimbra");
x.t("destination","server");
x.t("zimbra","server");
x.t("admin","name");
x.t("admin","services");
x.t("admin","port");
x.t("hostname","name");
x.t("enter","zimbra");
x.t("click","next");
x.t("configuration","destination");
x.t("services","run");
x.t("data","migrated");
x.t("port","7071");
x.t("port","admin");
x.t("port","port");
x.t("run","admin");
x.t("password","administrator");
x.t("password","password");
x.t("server","credentials");
x.t("server","admin");
x.t("server","click");
x.t("server","configuration");
x.t("server","data");
x.t("server","password");
}
