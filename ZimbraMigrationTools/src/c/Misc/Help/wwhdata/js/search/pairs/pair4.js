/*
 * ***** BEGIN LICENSE BLOCK *****
 * Zimbra Collaboration Suite CSharp Client
 * Copyright (C) 2012, 2014 Zimbra, Inc.
 * 
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "License");
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at: http://www.zimbra.com/license
 * The License is based on the Mozilla Public License Version 1.1 but Sections 14 and 15 
 * have been added to cover use of software over a computer network and provide for limited attribution 
 * for the Original Developer. In addition, Exhibit A has been modified to be consistent with Exhibit B. 
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. 
 * See the License for the specific language governing rights and limitations under the License. 
 * The Original Code is Zimbra Open Source Web Client. 
 * The Initial Developer of the Original Code is Zimbra, Inc. 
 * All portions of the code are Copyright (C) 2012, 2014 Zimbra, Inc. All Rights Reserved. 
 * ***** END LICENSE BLOCK *****
 */
function FileData_Pairs(x)
{
x.t("email","address");
x.t("files","destination");
x.t("create","account");
x.t("user","credentials");
x.t("secure","connections");
x.t("dialog","box");
x.t("administrator","create");
x.t("migration","account");
x.t("migration","tool");
x.t("443","port");
x.t("usually","port");
x.t("need","migration");
x.t("credentials","hostname");
x.t("account","email");
x.t("account","zcs");
x.t("account",".pst");
x.t("account","password");
x.t("box","enter");
x.t("name","destination");
x.t("entered","name@domain.com");
x.t("destination","dialog");
x.t("destination","zimbra");
x.t("destination","server");
x.t("address","entered");
x.t("address","address");
x.t("give","migration");
x.t("domain","name");
x.t("zimbra","server");
x.t("number","secure");
x.t("number","non-secure");
x.t("number","server");
x.t("hostname","domain");
x.t("non-secure","connections");
x.t("enter","user");
x.t("click","next");
x.t("configuration","administrator");
x.t("configuration","destination");
x.t("zcs","account");
x.t("zcs","server");
x.t("username","zcs");
x.t("connections","443");
x.t("connections","username");
x.t("tool","application");
x.t("information","need");
x.t("application","following");
x.t("port","number");
x.t("port","port");
x.t("following","information");
x.t(".pst","files");
x.t("name@domain.com","password");
x.t("password","click");
x.t("password","zcs");
x.t("server","usually");
x.t("server","give");
x.t("server","configuration");
x.t("server","port");
}
