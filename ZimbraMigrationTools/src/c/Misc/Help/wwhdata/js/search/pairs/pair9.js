/*
 * ***** BEGIN LICENSE BLOCK *****
 * Zimbra Collaboration Suite CSharp Client
 * Copyright (C) 2012, 2014 Zimbra, Inc.
 * 
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "License");
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at: http://www.zimbra.com/license
 * The License is based on the Mozilla Public License Version 1.1 but Sections 14 and 15 
 * have been added to cover use of software over a computer network and provide for limited attribution 
 * for the Original Developer. In addition, Exhibit A has been modified to be consistent with Exhibit B. 
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. 
 * See the License for the specific language governing rights and limitations under the License. 
 * The Original Code is Zimbra Open Source Web Client. 
 * The Initial Developer of the Original Code is Zimbra, Inc. 
 * All portions of the code are Copyright (C) 2012, 2014 Zimbra, Inc. All Rights Reserved. 
 * ***** END LICENSE BLOCK *****
 */
function FileData_Pairs(x)
{
x.t("double","click");
x.t("user","migrated");
x.t("user","account");
x.t("user","stop");
x.t("user","accounts");
x.t("dialog","view");
x.t("shows","state");
x.t("errors","warnings");
x.t("bar","shows");
x.t("state","migration");
x.t("migration","results");
x.t("migration","restart");
x.t("migration","tool");
x.t("migration","accompanied");
x.t("close","migration");
x.t("close","view");
x.t("open","log");
x.t("exit","close");
x.t("exit","click");
x.t("migrated","double");
x.t("migrated","status");
x.t("migrated","progress");
x.t("back","previous");
x.t("new","tab");
x.t("status","messages");
x.t("status","including");
x.t("list","users");
x.t("file","opens");
x.t("file","given");
x.t("results","dialog");
x.t("results","results");
x.t("results","viewing");
x.t("results","log");
x.t("warnings","view");
x.t("account","new");
x.t("account","log");
x.t("stop","migration");
x.t("stop","stop");
x.t("stop","click");
x.t("restart","back");
x.t("progress","errors");
x.t("progress","bar");
x.t("selected","account");
x.t("messages","user");
x.t("button","exit");
x.t("viewing","migration");
x.t("opens","appropriate");
x.t("opens","displaying");
x.t("given","user");
x.t("previous","view");
x.t("click","user");
x.t("click","exit");
x.t("click","stop");
x.t("click","migrate");
x.t("click","individual");
x.t("view","open");
x.t("view","results");
x.t("view","click");
x.t("view","accounts");
x.t("information","accounts");
x.t("appropriate","log");
x.t("displaying","selected");
x.t("migrate","button");
x.t("users","migrated");
x.t("accompanied","status");
x.t("individual","tab");
x.t("tab","close");
x.t("tab","opens");
x.t("tab","click");
x.t("accounts","migrated");
x.t("accounts","individual");
x.t("accounts","contains");
x.t("including","progress");
x.t("log","double");
x.t("log","file");
x.t("log","information");
x.t("contains","list");
}
